//
//  Hero.swift
//  Marvel
//
//  Created by Test User on 2019. 09. 13..
//  Copyright © 2019. NJE GAMF. All rights reserved.
//

import Foundation

class Hero {
    var name: String
    var image: String
    
    init(_ name: String, _ image: String) {
        self.name = name
        self.image = image
    }
}
