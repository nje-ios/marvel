//
//  ViewController.swift
//  Marvel
//
//  Created by Test User on 2019. 09. 13..
//  Copyright © 2019. NJE GAMF. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var heroes: [Hero] = []
    var current: Int = 0
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pagingLabel: UILabel!
    @IBOutlet weak var heroImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initHeroes()
    }

    func initHeroes() {
        heroes.append(Hero("Captain Marvel", "01"))
        heroes.append(Hero("Thanos", "02"))
        heroes.append(Hero("Black Panther", "03"))
        heroes.append(Hero("Yon-Rogg", "04"))
        heroes.append(Hero("Spider-Man", "05"))
        heroes.append(Hero("Goose the Cat", "06"))
        heroes.append(Hero("Iron Man", "07"))
        heroes.append(Hero("Captain America", "08"))
        heroes.append(Hero("Black Widow", "09"))
        heroes.append(Hero("Hulk", "10"))
        heroes.append(Hero("Hawkeye", "11"))
        heroes.append(Hero("Thor", "12"))
    }
    
    
    @IBAction func previousOnTouch(_ sender: UIButton) {
        displayHero(getPrevious())
    }
    
    
    @IBAction func nextOnTouch(_ sender: UIButton) {
        displayHero(getNext())
    }
    
    func getNext() -> Hero {
        current+=1
        if current >= heroes.count {
            current = 0
        }
        return heroes[current]
    }
    
    func getPrevious() -> Hero {
        current-=1
        if current < 0 {
            current = heroes.count - 1
        }
        return heroes[current]
    }
    
    func displayHero(_ hero: Hero) {
        nameLabel.text = hero.name
        pagingLabel.text = "\(current + 1)/\(heroes.count)"
        heroImageView.image = UIImage(named: hero.image)
    }
}

